{ stdenv, fetchzip, rustPlatform }:

rustPlatform.buildRustPackage rec {
  name = "webhook-proxy-${version}";
  version = "0.1.0";

  src = fetchzip {
    url = "https://gitlab.com/d6e/webhook-proxy/-/archive/${version}/${name}.zip";
    sha256 = "09dvy6m7x9rbcqc7nhzs96jykjh9mnwmaa2kwni69pj14cnpa58r";
  };
  cargoSha256 = "0q68qyl2h6i0qsz82z840myxlnjay8p1w5z7hfyr8fqp7wgwa9cx";

  checkPhase = ''
    runHook preCheck
    echo "Running cargo test --release"
    cargo test --release
    runHook postCheck
  '';
  meta = with stdenv.lib; {
    description = "A simple proxy to proxy gitlab webhooks.";
    homepage = https://gitlab.com/d6e/webhook-proxy;
    license = licenses.unlicense;
    platforms = platforms.all;
  };
}

