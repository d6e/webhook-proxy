use serde::{Deserialize, Serialize};
use warp;
use warp::Filter;
use regex::Regex;


#[derive(Serialize, Deserialize)]
struct GitlabMsg {
    content: String,
    username: Option<String>,
    avatar_url: Option<String>,
    tts: bool,
    embeds: Vec<String>
}

fn fix_gitlab_bug(mut msg: GitlabMsg) -> String {
    println!("Old msg.content={}", msg.content);
    let re = Regex::new(r"\(http.?://[^ )]*\)").unwrap();  // grab all urls surrounded with parenthesis
    let mut urls: Vec<(String, String)> = Vec::new();
    for cap in re.captures_iter(&msg.content) {
        let url:&str = &cap[0];
        let prepended_url = url.replace('(', "(<");
        let angle_bracketed_url = prepended_url.replace(')', ">)");
        urls.push((url.to_string(), angle_bracketed_url));
    }
    for url in urls {
        let (old_url, new_url) = url;
        msg.content = msg.content.replace(&old_url, &new_url);
    }
    println!("New msg.content={}", msg.content);
    serde_json::to_string(&msg).unwrap()
}

#[tokio::main]
async fn main() {

    let routes = warp::path!("discord" / String)
        .and(warp::body::content_length_limit(1024 * 32))
        .and(warp::body::json())
        .map(|_, body: GitlabMsg | {
            fix_gitlab_bug(body)
        });

    warp::serve(routes).run(([127, 0, 0, 1], 8888)).await;
}
